import io
import os
import json
import logging
import urllib2
import time

from flask import Flask, request
from flask_cors import CORS

import sprint_client

logging.basicConfig(level=logging.DEBUG)

app = Flask(__name__)
CORS(app)

@app.route("/")
def hello():
  return "api server v1.0"

@app.route("/smart-printing/1.0/reporting/user/<user_id>", methods=['GET'])
def reporting_user(user_id):
    logging.info("reporting_user.1")
    ret_val = {"status": "ko", 'error': 'invalid_request'}
    try:
        sp_client = sprint_client.SmartPrintingClient.get_instance()
        user_report = sp_client.get_report_user(user_id)
        ret_val = {"status": "ok", 'data': user_report}
    except Exception as e:
        logging.info("Exception: " + str(e))
    return json.dumps(ret_val)

@app.route("/smart-printing/1.0/reporting/summary", methods=['GET'])
def reporting_summary():
    logging.info("reporting_summary.1")
    ret_val = {"status": "ko", 'message': 'invalid_request'}
    try:
        sp_client = sprint_client.SmartPrintingClient.get_instance()
        summary_report = sp_client.get_report_summary()
        ret_val = {"status": "ok", 'data': summary_report}
    except Exception as e:
        logging.info("Exception: " + str(e))
    return json.dumps(ret_val)

@app.route("/smart-printing/1.0/reporting/score/users", methods=['GET'])
def reporting_users_score():
    logging.info("reporting_users_score.1")
    ret_val = {"status": "ko", 'message': 'invalid_request'}
    try:
        sp_client = sprint_client.SmartPrintingClient.get_instance()
        users_score = sp_client.get_report_users_score()
        ret_val = {"status": "ok", 'data': users_score}
    except Exception as e:
        logging.info("Exception: " + str(e))
    return json.dumps(ret_val)

@app.route("/swagger")
def test():
  return app.send_static_file('api_sprint_swagger.yaml')

if __name__ == "__main__":
    sp_client = sprint_client.SmartPrintingClient.get_instance()
    sp_client.init_stats()
    sp_client.start()
    app.run(host='0.0.0.0',port=5000,threaded=True)
