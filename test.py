import unittest
import sys
import datetime
import time
import logging
import json
import sprint_client

logging.basicConfig(level=logging.DEBUG)

user = None

class TestSmartPrintClient(unittest.TestCase):
  def test_client(self):
    client = sprint_client.SmartPrintingClient.get_instance()
    client.init_stats()
    report_user = client.get_report_user(user)
    report_summary = client.get_report_summary()

    logging.info(json.dumps(report_user))
    logging.info(json.dumps(report_summary))
    self.assertTrue(report_user)

if __name__ == '__main__':
  if len(sys.argv) < 2:
    print "usage: " + sys.argv[0] + " user_id"
    exit()
  user = sys.argv[1]
  sys.argv = sys.argv[:1]
  unittest.main()
