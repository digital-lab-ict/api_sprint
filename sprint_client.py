import os
import datetime
import time
import sets
import threading
import logging
import cx_Oracle

DATE_FORMAT = "%Y%m%d"

class SmartPrintingClient(threading.Thread):
    instance = None

    @classmethod
    def get_instance(cls):
        instance = cls.instance
        if instance is None:
            instance = SmartPrintingClient()
            cls.instance = instance
        return instance

    def __init__(self):
        server = os.getenv("SP_DB_SERVER")
        port = os.getenv("SP_DB_PORT")
        db = os.getenv("SP_DB_DATABASE")
        user = os.getenv("SP_DB_USERNAME")
        password = os.getenv("SP_DB_PASSWORD")
        start_time = os.environ["REFRESH_START_TIME"]

        self.start_time = datetime.datetime.strptime(start_time, '%H:%M')
        dsn_tns = cx_Oracle.makedsn(server, port, db)

        self.conn = cx_Oracle.connect(user, password, dsn_tns)
        self.lock = threading.RLock()
        self._running = True
        super(SmartPrintingClient, self).__init__()

    def run(self):
        while self._running:
            now = datetime.datetime.now()
            td = (now - self.start_time).seconds / 60
            if td >= 0 and td < 60:
                self.init_stats()
                time.sleep(3600)
            time.sleep(60)

    def init_stats(self):
        today = datetime.date.today()
        time_start = today - datetime.timedelta(days=365)
        time_end = today + datetime.timedelta(days=1)
        copies = self.query_copy(None, time_start, time_end)
        prints = self.query_print(None, time_start, time_end)
        faxes = self.query_fax(None, time_start, time_end)
        scans = self.query_scan(None, time_start, time_end)

        data = {"print": prints, "copy": copies, "scan": scans, "fax": faxes}
        # tx_summary structure:
        # tx_summaries["<type>"]["<user_id>"|"all"]["months"]=TransactionSummary()
        users_score = {}
        tx_summaries = {}
        tx_summaries["all"] = {}
        for t in ["all", "print", "copy", "scan", "fax"]:
            tx_summaries[t] = {}
            tx_summaries[t]["all"] = {}
            users_score[t] = {}

        tx_summary_all = tx_summaries["all"]["all"]

        for type, transactions in data.iteritems():
            logging.info("type: " + type + " len: " + str(len(data[type])))
            tx_summary_type = tx_summaries[type]["all"]
            for c in transactions:
                tx_summary_user = tx_summaries[type].get(c.user_id)
                if tx_summary_user is None:
                    tx_summary_user = {}
                    tx_summaries[type][c.user_id] = tx_summary_user
                    tx_summary_user["all"] = self.TransactionSummary(c.user_id, type, "all", "y")

                if users_score[type].get(c.user_id) is None:
                    users_score[type][c.user_id] = 0

                tx_summary_date = datetime.datetime.strftime(c.date.replace(day=1), DATE_FORMAT)

                tx_summary_user_date = tx_summary_user.get(tx_summary_date)
                if tx_summary_user_date is None:
                    tx_summary_user_date = self.TransactionSummary(c.user_id, type, tx_summary_date, "m")
                    tx_summary_user[tx_summary_date] = tx_summary_user_date
                tx_summary_user_date.amount += c.amount
                users_score[type][c.user_id] += c.amount

                tx_summary_all_date = tx_summary_all.get(tx_summary_date)
                if tx_summary_all_date is None:
                    tx_summary_all_date = self.TransactionSummary(None, "all", tx_summary_date, "m")
                    tx_summary_all[tx_summary_date] = tx_summary_all_date
                tx_summary_all_date.amount += c.amount
                tx_summary_all_date.users.add(c.user_id)

                tx_summary_type_date = tx_summary_type.get(tx_summary_date)
                if tx_summary_type_date is None:
                    tx_summary_type_date = self.TransactionSummary(None, type, tx_summary_date, "m")
                    tx_summary_type[tx_summary_date] = tx_summary_type_date
                tx_summary_type_date.amount += c.amount
                tx_summary_type_date.users.add(c.user_id)

        self.lock.acquire()
        self.users_score = users_score
        self.tx_summaries = tx_summaries
        self.lock.release()


    def query_copy(self, user_id, time_start, time_end):
        cursor = self.conn.cursor()
        if user_id:
            cursor.execute("SELECT accountname, trxdate, pagecount FROM eqcas.view_copy WHERE accountname=:1 AND trxdate>=:2 AND trxdate<:3", (user_id, time_start, time_end))
        else:
            cursor.execute("SELECT accountname, trxdate, pagecount FROM eqcas.view_copy WHERE trxdate>=:1 AND trxdate<:2", (time_start, time_end))

        results = []
        row = cursor.fetchone()
        while row:
            user_id_no_domain = row[0] if row[0].find('\\') == -1 else row[0][row[0].rfind('\\')+1:]
            results.append(self.Transaction(user_id=user_id_no_domain, type="copy", date=row[1], amount=row[2]))
            row = cursor.fetchone()
        return results

    def query_scan(self, user_id, time_start, time_end):
        cursor = self.conn.cursor()
        if user_id:
            cursor.execute("SELECT accountname, trxdate, pagecount FROM eqcas.view_scan WHERE accountname=:1 AND trxdate>=:2 AND trxdate<:3", (user_id, time_start, time_end))
        else:
            cursor.execute("SELECT accountname, trxdate, pagecount FROM eqcas.view_scan WHERE trxdate>=:1 AND trxdate<:2", (time_start, time_end,))

        results = []
        row = cursor.fetchone()
        while row:
            user_id_no_domain = row[0] if row[0].find('\\') == -1 else row[0][row[0].rfind('\\')+1:]
            results.append(self.Transaction(user_id=user_id_no_domain, type="scan", date=row[1], amount=row[2]))
            row = cursor.fetchone()
        return results

    def query_print(self, user_id, time_start, time_end):
        cursor = self.conn.cursor()
        if user_id:
            cursor.execute("SELECT accountname, trxdate, pagecount FROM eqcas.view_print WHERE accountname=:1 AND trxdate>=:2 AND trxdate<:3 AND pagesize != 'Custom'", (user_id, time_start, time_end))
        else:
            cursor.execute("SELECT accountname, trxdate, pagecount FROM eqcas.view_print WHERE trxdate>=:1 AND trxdate<:2 AND pagesize != 'Custom'", (time_start, time_end))

        results = []
        row = cursor.fetchone()
        while row:
            user_id_no_domain = row[0] if row[0].find('\\') == -1 else row[0][row[0].rfind('\\')+1:]
            results.append(self.Transaction(user_id=user_id_no_domain, type="print", date=row[1], amount=row[2]))
            row = cursor.fetchone()
        return results

    def query_fax(self, user_id, time_start, time_end):
        cursor = self.conn.cursor()
        if user_id:
            cursor.execute("SELECT accountname, trxdate, pagecount FROM eqcas.view_fax WHERE accountname=:1 AND trxdate>=:2 AND trxdate<:3", (user_id, time_start, time_end))
        else:
            cursor.execute("SELECT accountname, trxdate, pagecount FROM eqcas.view_fax WHERE trxdate>=:1 AND trxdate<:2", (time_start, time_end))

        results = []
        row = cursor.fetchone()
        while row:
            user_id_no_domain = row[0] if row[0].find('\\') == -1 else row[0][row[0].rfind('\\')+1:]
            results.append(self.Transaction(user_id=user_id_no_domain, type="fax", date=row[1], amount=row[2]))
            row = cursor.fetchone()
        return results

    def get_report_user(self, user_id):
        report = {}
        user_id = user_id.upper()
        for t in ["all", "print", "copy", "scan", "fax"]:
            summary = self.tx_summaries.get(t)
            if summary:
                report[t] = {}
                for d, s in summary.get(user_id, {}).iteritems():
                    report[t][d] = s.as_dict()
        return report

    def get_report_summary(self):
        report = {}
        self.lock.acquire()
        for t in ["all", "print", "copy", "scan", "fax"]:
            summary = self.tx_summaries.get(t)
            if summary:
                report[t] = {}
                for d, s in summary.get("all", {}).iteritems():
                    report[t][d] = s.as_dict()
        self.lock.release()
        return report

    def get_report_users_score(self):
        self.lock.acquire()
        users_score = self.users_score
        self.lock.release()
        return users_score

    class Transaction:
        def __init__(self, user_id, type, date, amount):
            self.user_id = user_id.upper() if user_id else None
            self.type = type
            self.date = date
            self.amount = amount

    class TransactionSummary:
        def __init__(self, user_id, type, date, span):
            self.user_id = user_id.upper() if user_id else None
            self.users = sets.Set()
            self.type = type
            self.date = date
            self.span = span
            self.amount = 0

        def as_dict(self):
            return {"user_id": self.user_id,
                    "users": len(self.users),
                    "type": self.type,
                    "date": self.date,
                    "span": self.span,
                    "amount": self.amount}
